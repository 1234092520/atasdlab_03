﻿using System;
using System.Diagnostics;

class Program
{
    static void Main()
    {
        Console.WriteLine("n\tFn(n)\tLog(n)\tNLogN(n)\tSquare(n)\tTwoPowerN(n)\tFactorial(n)");

        for (int n = 0; n <= 50; n++)
        {
            double fn = Fn(n);
            double log = Log(n);
            double nlogn = NLogN(n);
            double square = Square(n);
            double twoPowerN = TwoPowerN(n);
            double factorial = Factorial(n);

          
            fn = Math.Min(fn, 500);
            log = Math.Min(log, 500);
            nlogn = Math.Min(nlogn, 500);
            square = Math.Min(square, 500);
            twoPowerN = Math.Min(twoPowerN, 500);
            factorial = Math.Min(factorial, 500);

            Console.WriteLine($"{n}\t{fn}\t{log:F4}\t{nlogn:F4}\t{square}\t{twoPowerN}\t{factorial}");
        }
    }

    static int Fn(int n) => n;
    static double Log(int n) => n > 0 ? Math.Log(n) : 0;
    static double NLogN(int n) => n * Log(n);
    static double Square(int n) => Math.Pow(n, 2);
    static double TwoPowerN(int n) => Math.Pow(2, n);
    static double Factorial(int n)
    {
        if (n <= 1) return 1;
        double result = 1;
        for (int i = 2; i <= n; i++)
        {
            result *= i;
        }
        return result;
    }
}
