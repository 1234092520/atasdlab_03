﻿using System;
using System.Diagnostics;

class Program
{
    static void Main()
    {
        var array = new int[1000]; 
        FillArray(array);

        Console.WriteLine("Initial array:");
        PrintArray(array);

        ReverseBubbleSort(array);

        Console.WriteLine("Sorted array in descending order:");
        PrintArray(array);
    }

    static void FillArray(int[] array)
    {
        var rand = new Random();
        for (int i = 0; i < array.Length; i++)
        {
            array[i] = rand.Next(20001) - 10000;
        }
    }

    static void ReverseBubbleSort(int[] array)
    {
        var time = new Stopwatch();
        time.Start();
        for (int j = 0; j <= array.Length - 2; j++)
        {
            for (int i = 0; i <= array.Length - 2 - j; i++)
            {
                if (array[i] < array[i + 1])
                {
                    int temp = array[i + 1];
                    array[i + 1] = array[i];
                    array[i] = temp;
                }
            }
        }
        time.Stop();
        Console.WriteLine($"Total elapsed time: {1000000000 * time.ElapsedTicks / Stopwatch.Frequency} ns");
    }

    static void PrintArray(int[] array)
    {
        foreach (var item in array)
        {
            Console.Write($"{item} ");
        }
        Console.WriteLine();
    }
}
