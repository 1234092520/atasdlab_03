﻿using System;
using System.Diagnostics;

class Program
{
    static void Main(string[] args)
    {
        Stopwatch time = new Stopwatch();
        for (int n = 1; n <= 90; n++)
        {
            time.Start();
            double result = GetFibonacci(n);
            time.Stop();
            Console.WriteLine($"{1000000000 * time.ElapsedTicks / Stopwatch.Frequency}");
            time.Reset();
        }
    }

    static double GetFibonacci(int n)
    {
        if (n <= 1)
            return n;

        double res = 1;
        double prevNum = 0;

        for (int i = 2; i <= n; i++)
        {
            double temp = res;
            res += prevNum;
            prevNum = temp;
        }

        return res;
    }
}
